import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Routes } from './Routes';
import { StackNavigation } from './StackNavigation';

export type RootStackParamList = {
    [Routes.DemoSlider]: undefined;
    [Routes.Welcome]: undefined;
};

export const Stack = createStackNavigator<RootStackParamList>();

const RootNavigation = () => {
	return (
		<NavigationContainer>
			<StackNavigation />
		</NavigationContainer>
	);
};

export default RootNavigation;
