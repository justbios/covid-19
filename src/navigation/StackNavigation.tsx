import React from 'react';
import { Stack } from '.';
import DemoSlider from '../screens/Demo';
import Welcome from '../screens/Welcome';
import { Routes } from './Routes';

export const StackNavigation = () => {
	return (
		<Stack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<Stack.Screen name={Routes.DemoSlider} component={DemoSlider} />
			<Stack.Screen name={Routes.Welcome} component={Welcome} />
		</Stack.Navigator>
	);
};
