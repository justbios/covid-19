import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { Routes } from '../../navigation/Routes';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../navigation';
import { SafeAreaView } from 'react-native-safe-area-context';
import SliderItem from '../../components/SliderItem';

const data = [
	{
		image: '../../assets/images/Thumbnail_01.png',
		title: 'Stay safe during the pandemic',
		description:
            'Control having the list of necessary things in different satuations'
	},
	{
		image: '../../assets/images/Thumbnail_02.png',
		title: 'Check your security level',
		description:
            'Be honest with yourself and take care of you dearest&nearest'
	},
	{
		image: '../../assets/images/Thumbnail_03.png',
		title: 'Find advice and recommendations',
		description: 'To Stay Safe when things around are complicated'
	}
];

const DemoSlider: FC<
    NativeStackScreenProps<RootStackParamList, Routes.DemoSlider>
> = () => {
	return (
		<SafeAreaView>
			<View style={styles.container}>
				<SliderItem
					image={data[0].image}
					title={data[0].title}
					description={data[0].description}
				/>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		alignSelf: 'center'
	}
});

export default DemoSlider;
