import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { FC } from 'react';
import { Button, Text } from 'react-native';
import { RootStackParamList } from '../../navigation';
import { Routes } from '../../navigation/Routes';

const Welcome: FC<
    NativeStackScreenProps<RootStackParamList, Routes.Welcome>
> = () => {
	return (
		<>
			<Text>Welcome</Text>
			<Button title="hello" />
		</>
	);
};

export default Welcome;
