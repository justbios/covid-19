import React, { PropsWithChildren, FC } from 'react';
import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { colors } from '../../utils/Colors';

export enum ButtonType {
    Primary = 'primary',
    Secondary = 'secondary'
}

interface IButtonProps {
    type?: ButtonType;
    onPress: () => void;
    style?: StyleProp<ViewStyle>;
}

const Button: FC<PropsWithChildren<IButtonProps>> = ({
	type = ButtonType.Primary,
	onPress,
	children,
	style
}) => {
	return (
		<TouchableOpacity onPress={onPress}>
			<View style={[styles.container, styles[type], style]}>
				{children}
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		borderColor: colors.blue,
		borderWidth: 2,
		borderRadius: 8,
		width: 310,
		paddingVertical: 15,
		alignItems: 'center',
		alignSelf: 'center'
	},
	primary: {
		backgroundColor: 'transparent'
	},
	secondary: {
		backgroundColor: colors.blue
	}
});

export default Button;
