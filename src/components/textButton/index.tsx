import React, { FC } from 'react';
import { StyleProp, Text, TouchableOpacity, ViewStyle } from 'react-native';
import { colors } from '../../utils/Colors';

interface ITextButtonProps {
    onPress: () => void;
    color?: string;
    text: string;
    style?: StyleProp<ViewStyle>;
}

const TextButton: FC<ITextButtonProps> = ({
	onPress,
	color = colors.grey,
	text,
	style
}) => {
	return (
		<TouchableOpacity onPress={onPress}>
			<Text style={[{ color: color, fontSize: 17 }, style]}>{text}</Text>
		</TouchableOpacity>
	);
};

export default TextButton;
