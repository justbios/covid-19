import React, { FC } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { colors } from '../../utils/Colors';

interface ISliderItemProps {
    image: string;
    title: string;
    description: string;
}

const SliderItem: FC<ISliderItemProps> = ({ image, title, description }) => {
	return (
		<View style={styles.container}>
			<Image
				style={styles.image}
				source={require('../../assets/images/Thumbnail_01.png')}
			/>
			<Text style={styles.title}>{title}</Text>
			<Text style={styles.description}>{description}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 10
	},
	image: {
		marginBottom: 40
	},
	title: {
		fontSize: 35,
		fontWeight: 'bold',
		marginBottom: 28
	},
	description: {
		fontSize: 17,
		color: colors.grey
	}
});

export default SliderItem;
