import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import RootNavigation from './src/navigation';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';

const App = () => {
	useEffect(() => {
		SplashScreen.hide();
	}, []);

	return (
		<SafeAreaProvider>
			<RootNavigation />
		</SafeAreaProvider>
	);
};

export default App;
